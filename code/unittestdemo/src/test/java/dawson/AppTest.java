package dawson;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void echoTest(){
        assertEquals("echo should return what it's given", 5, App.echo(5));
    }

    @Test
    public void oneMoreTest(){
        assertEquals("oneMore should return what it's given + 1", 6, App.oneMore(5));
    }
}
